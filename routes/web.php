<?php

use App\Http\Controllers\Admin\CategoryController;

Route::get('/', 'HomeController@index');
Route::get('/article/{id}', 'HomeController@show');
Route::get('/rss.xml', 'HomeController@rss');
Route::get('/sitemap.xml', 'HomeController@sitemap');
Route::get('/login', 'AuthController@getLogin');
Route::get('/forgot-password', 'AuthController@getForgotPassword');
Route::post('/forgot-password', 'AuthController@postForgotPassword');
Route::post('/login', 'AuthController@postLogin');
Route::get('/logout', 'AuthController@getLogout');
Route::post('/reset_password', 'AuthController@postReset');
Route::get('/reset_password/{email}/{reset_code}', 'AuthController@getReset');
Route::post('/forgot_password', 'AuthController@getForgotPassword');
//Error Handler
Route::get('/403', 'HomeController@show403');
Route::get('/404', 'HomeController@show404');
Route::get('/500', 'HomeController@show500');
Route::get('/503', 'HomeController@show503');
//Site Routes
Route::get('/page/{page_slug}', 'HomeController@page');
Route::get('/author/{author_slug}', 'HomeController@byAuthor');
Route::get('/category/{category_slug}', 'HomeController@byCategory');
Route::get('/category/{category_slug}/{sub_category_slug}', 'HomeController@bySubCategory');
Route::get('/tag/{tag_slug}', 'HomeController@byTag');
Route::get('/search', 'HomeController@bySearch');
Route::get('/rss.xml', 'HomeController@rss');
Route::get('/rss', 'HomeController@rss');
Route::get('/rss/{category_slug}', 'HomeController@categoryRss');
Route::get('/rss/{category_slug}/{sub_category_slug}', 'HomeController@subCategoryRss');
Route::get('/sitemap.xml', 'HomeController@sitemap');
Route::post('/submit_rating', 'HomeController@submitRating');
Route::post('/submit_likes', 'HomeController@submitLike');
//Admin Routes
Route::group(array('namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'), function (): void {
    Route::get('/', 'DashboardController@index');
    Route::get('/update_application', 'DashboardController@updateApplication');
    Route::group(array('prefix' => 'api'), function (): void {
        Route::get('/get_tags', 'APIController@getTags');
    });
    Route::resource('crons', 'CronController');

    Route::group(array('prefix' => 'users'), function (): void {
        Route::get('/', 'UsersController@all');
        Route::get('/all', 'UsersController@all');
        Route::get('/create', 'UsersController@create');
        Route::get('/edit/{id}', 'UsersController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'UsersController@delete')->where(array('id' => '[0-9]+'));
        ;
        Route::post('/create', 'UsersController@store');
        Route::post('/update', 'UsersController@update');
    });
    Route::resource('categories', CategoryController::class);

    Route::group(array('prefix' => 'sources'), function (): void {
        Route::get('/', 'SourcesController@all');
        Route::get('/all', 'SourcesController@all');
        Route::get('/pull_feeds', 'SourcesController@pullFeeds');
        Route::get('/pull_page', 'SourcesController@pullPages');
        Route::get('/create', 'SourcesController@create');
        Route::get('/edit/{id}', 'SourcesController@edit')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/refresh/{id}', 'SourcesController@refresh')->where(array('id' => '[0-9]+'));
        ;
        Route::get('/delete/{id}', 'SourcesController@delete')->where(array('id' => '[0-9]+'));
        ;
        Route::post('/create', 'SourcesController@store');
        Route::post('/update', 'SourcesController@update');
    });
    Route::group(array('prefix' => 'posts'), function (): void {
        Route::get('/', 'PostsController@all');
        Route::get('/all', 'PostsController@all');
        Route::get('/create', 'PostsController@create');
        Route::get('/edit/{id}', 'PostsController@edit')->where(array('id' => '[0-9]+'));
        Route::get('/delete/{id}', 'PostsController@delete')->where(array('id' => '[0-9]+'));
        Route::post('/create', 'PostsController@store');
        Route::post('/update', 'PostsController@update');
    });
    Route::get('/post/delete', 'PostsController@multiDelete');

    Route::group(array('prefix' => 'ratings'), function (): void {
        Route::get('/', 'RatingsController@all');
        Route::get('/all', 'RatingsController@all');
        Route::get('/delete/{id}', 'RatingsController@delete')->where(array('id' => '[0-9]+'));
    });
    Route::group(array('prefix' => 'tags'), function (): void {
        Route::get('/', 'TagsController@all');
        Route::get('/all', 'TagsController@all');
        Route::get('/delete/{id}', 'TagsController@delete')->where(array('id' => '[0-9]+'));
    });
    Route::get('/tag/delete', 'TagsController@multiDelete');
    Route::resource('pages', 'PagesController');
    Route::resource('ads', 'AdsController');
    Route::group(array('prefix' => 'statistics'), function (): void {
        Route::get('/', 'StatisticsController@all');
        Route::get('/all', 'StatisticsController@all');
    });
    Route::group(array('prefix' => 'settings'), function (): void {
        Route::get('/', 'SettingsController@all');
        Route::get('/all', 'SettingsController@all');
        Route::get('delete_manually/{days}', 'SettingsController@deleteOldManually');
        Route::post('update_custom_css', 'SettingsController@updateCustomCSS');
        Route::post('update_custom_js', 'SettingsController@updateCustomJS');
        Route::post('update_social', 'SettingsController@updateSocial');
        Route::post('update_comments', 'SettingsController@updateComments');
        Route::post('update_seo', 'SettingsController@updateSEO');
        Route::post('update_general', 'SettingsController@updateGeneral');
        Route::post('update_payments', 'SettingsController@updatePayments');
        Route::post('delete_old_news', 'SettingsController@updateDeleteNews');
    });
});
Route::get('/admin/api/get_sub_categories_by_category/{id}', 'Admin\APIController@getSubCategories');
Route::get('/admin/redactor/images.json', 'Admin\DashboardController@redactorImages');
Route::post('/admin/redactor', 'Admin\DashboardController@handleRedactorUploads');
Route::get('/api/categories', 'HomeController@categories');
Route::get('/api/subcategories', 'HomeController@subcategories');
Route::get('/api/sources', 'HomeController@sources');
Route::get('/api/posts', 'HomeController@posts');
Route::get('/api/image/posts', 'HomeController@imagePosts');
Route::get('/api/text/posts', 'HomeController@textPosts');
Route::get('/api/video/posts', 'HomeController@videoPosts');
Route::get('/api/tags', 'HomeController@tags');
Route::get('/api/settings', 'HomeController@settings');
//should be last route
Route::get('/{article_slug}', 'HomeController@article');
