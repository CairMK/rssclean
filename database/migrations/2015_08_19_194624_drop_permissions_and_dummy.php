<?php

use Illuminate\Database\Migrations\Migration;

class DropPermissionsAndDummy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists("dummy");
        Schema::dropIfExists("permissions");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
