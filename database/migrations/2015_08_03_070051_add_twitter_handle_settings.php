<?php

use App\Models\Setting;
use Illuminate\Database\Migrations\Migration;

class AddTwitterHandleSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $settings = new Setting();
        $settings->category = Setting::CATEGORY_SOCIAL;
        $settings->column_key = 'twitter_handle';
        $settings->value_string = '';
        $settings->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Setting::where('category', Setting::CATEGORY_SOCIAL)->where('column_key', 'twitter_handle')->delete();
    }
}
