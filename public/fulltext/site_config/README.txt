Full-Text RSS Site Patterns
---------------------------

Site patterns allow you to specify what should be extracted from specific sites.

Please see https://help.fivefilters.org/full-text-rss/site-patterns.html for more information.