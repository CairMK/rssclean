<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Rss\Rss;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton('rss', function ($app): Rss {
            return new Rss();
        });

    }
}
