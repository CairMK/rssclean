<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePageRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required'],
            'slug' => ['required'],
            'description' => ['required'],
            'show_in_menu' => ['required', 'integer'],
            'show_in_sidebar' => ['required', 'integer'],
            'show_in_footer' => ['required', 'integer'],
            'seo_keywords' => ['required'],
            'seo_description' => ['required'],
            'status' => ['required', 'integer'],
            'author_id' => ['required', 'exists:users'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
