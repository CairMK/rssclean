<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * Adjust this logic as needed.
     */
    public function authorize(): true
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title'             => 'required|string|max:255|unique:categories',
            'scroll_type'       => 'nullable|string',
            'seo_keywords'      => 'nullable|string|max:255',
            'seo_description'   => 'nullable|string',
            'show_in_menu'      => 'sometimes|boolean',
            'show_in_sidebar'   => 'sometimes|boolean',
            'show_in_footer'    => 'sometimes|boolean',
            'show_as_mega_menu' => 'sometimes|boolean',
            'show_on_home'      => 'sometimes|boolean',
        ];
    }
}
