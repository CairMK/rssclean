<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'author_id' => ['nullable ', 'exists:users'],
            'title' => ['nullable'],
            'slug' => ['nullable'],
            'link' => ['nullable'],
            'featured' => ['required', 'integer'],
            'category_id' => ['required', 'exists:categories'],
            'type' => ['required'],
            'source_id' => ['required', 'exists:sources'],
            'description' => ['required'],
            'featured_image' => ['required'],
            'views' => ['required', 'integer'],
            'status' => ['required', 'integer'],
            'show_in_mega_menu' => ['required', 'integer'],
            'render_type' => ['required'],
            'video_embed_code' => ['required'],
            'image_parallax' => ['required', 'integer'],
            'video_parallax' => ['required', 'integer'],
            'rating_box' => ['required', 'integer'],
            'show_featured_image_in_post' => ['required', 'integer'],
            'show_author_box' => ['required', 'integer'],
            'show_author_socials' => ['required', 'integer'],
            'dont_show_author_publisher' => ['required', 'integer'],
            'show_post_source' => ['required', 'integer'],
            'rating_desc' => ['required'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
