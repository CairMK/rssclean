<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'code' => 'required|string',
            'position' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
