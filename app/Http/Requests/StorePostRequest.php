<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'author_id' => ['required', 'exists:users'],
            'title' => ['required'],
            'slug' => ['required'],
            'link' => ['required'],
            'featured' => ['required', 'integer'],
            'category_id' => ['required', 'exists:categories'],
            'type' => ['required'],
            'source_id' => ['required', 'exists:sources'],
            'description' => ['required'],
            'featured_image' => ['required'],
            'views' => ['required', 'integer'],
            'status' => ['required', 'integer'],
            'show_in_mega_menu' =>  ['integer', 'nullable', 'default:0'],
            'render_type' => ['integer', 'nullable', 'default:0'],
            'video_embed_code' => [ 'integer', 'nullable', 'default:0'],
            'image_parallax' => [ 'integer', 'nullable', 'default:0'],
            'video_parallax' => [ 'integer', 'nullable', 'default:0'],
            'rating_box' => ['integer', 'nullable', 'default:0'],
            'show_featured_image_in_post' => [ 'integer', 'nullable', 'default:0'],
            'show_author_box' => [ 'integer', 'nullable', 'default:0'],
            'show_author_socials' => [ 'integer', 'nullable', 'default:0'],
            'dont_show_author_publisher' => ['integer', 'nullable', 'default:0'],
            'show_post_source' => ['integer', 'nullable', 'default:0'],
            'rating_desc' => ['required'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
