<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSourceRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'url' => ['required'],
            'priority' => ['required', 'integer'],
            'category_id' => ['required', 'exists:categories'],
            'channel_title' => ['required'],
            'channel_link' => ['required'],
            'channel_description' => ['required'],
            'channel_language' => ['required'],
            'channel_pubDate' => ['required'],
            'channel_lastBuildDate' => ['required'],
            'channel_generator' => ['required'],
            'auto_update' => ['required', 'integer'],
            'items_count' => ['required', 'integer'],
            'dont_show_author_publisher' => ['required', 'integer'],
            'show_post_source' => ['required', 'integer'],
            'fetch_full_text' => ['required', 'integer'],
            'use_auto_spin' => ['required', 'integer'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
