<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Category;
use App\Models\Page;
use App\Models\Post;
use App\Models\PostRating;
use App\Models\Setting;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Libraries\Utils;

class BaseController extends Controller
{
    use DispatchesJobs;
    use ValidatesRequests;

    public array $data = [];

    public function __construct()
    {

        $featured_ids = Post::where('render_type', '!=', Post::RENDER_TYPE_TEXT)->orderBy('created_at', 'desc')->where('status', Post::STATUS_PUBLISHED)->limit(6)->pluck('id');


        $this->data['settings_general'] = Utils::getSettings(Setting::CATEGORY_GENERAL);
        $this->data['settings_seo'] = Utils::getSettings(Setting::CATEGORY_SEO);
        $this->data['settings_comments'] = Utils::getSettings(Setting::CATEGORY_COMMENTS);
        $this->data['settings_social'] = Utils::getSettings(Setting::CATEGORY_SOCIAL);
        $this->data['settings_custom_js'] = Utils::getSettings(Setting::CATEGORY_CUSTOM_JS);
        $this->data['settings_custom_css'] = Utils::getSettings(Setting::CATEGORY_CUSTOM_CSS);
        $this->data['just_posted'] = Post::orderBy('created_at', 'desc')->whereNotIn('id', $featured_ids)->where('render_type', Post::RENDER_TYPE_TEXT)->where('status', Post::STATUS_PUBLISHED)->limit(5)->get();


        $this->data['global_cats'] = Category::all();
        $this->data['global_pages'] = Page::where('status', Post::STATUS_PUBLISHED)->get();

        foreach ($this->data['global_pages'] as $page) {
            $page->author = User::where('id', $page->author_id)->first();
        }

        foreach ($this->data['global_cats'] as $cat) {

            $cat->post_count = Post::whereIn('category_id', $cat->sub_categories->pluck('id')->toArray())->count();

            if (sizeof($cat->sub_categories) > 0) {
                foreach ($cat->sub_categories as $sub_cat) {
                    $sub_cat->mega_menu_posts = Post::where('show_in_mega_menu', 1)->where('render_type', '!=', Post::RENDER_TYPE_TEXT)->where('category_id', $sub_cat->id)->limit(4)->get();

                    if (sizeof($sub_cat->mega_menu_posts) > 0) {
                        foreach ($sub_cat->mega_menu_posts as $post) {
                            if ($post->rating_box == 1) {
                                $all_ratings = PostRating::orderBy('created_at', 'desc')->where('post_id', $post->id)->where('approved', 1)->pluck('rating');

                                if (sizeof($all_ratings) > 0) {

                                    $total = 0;

                                    foreach ($all_ratings as $rating) {
                                        $total = $total + $rating;
                                    }

                                    $post->average_rating = (float)($total / sizeof($all_ratings));

                                } else {
                                    $post->average_rating = 0;
                                }
                            }
                        }
                    }

                }
            }

        }



        $this->data['popular_tags'] = Tag::orderBy('views', 'desc')->limit(10)->groupBy('title')->distinct()->get();


        $this->data['review_posts'] = Post::orderBy('created_at', 'desc')->orderBy('views', 'desc')->where('render_type', '!=', Post::RENDER_TYPE_TEXT)->where('rating_box', 1)->where('status', Post::STATUS_PUBLISHED)->limit(20)->get();

        foreach ($this->data['review_posts'] as $post) {

            $post->author = User::where('id', $post->author_id)->first();
            $post->category = Category::where('id', $post->sub_category->parent_id)->first();

            $all_ratings = PostRating::orderBy('created_at', 'desc')->where('post_id', $post->id)->where('approved', 1)->pluck('rating');

            if (sizeof($all_ratings) > 0) {

                $total = 0;

                foreach ($all_ratings as $rating) {
                    $total = $total + $rating;
                }

                $post->average_rating = (float)($total / sizeof($all_ratings));

            } else {
                $post->average_rating = 0;
            }

        }


    }

    public function throw404()
    {
        return redirect()->to('404');
    }

    public function show403()
    {
        $this->data['ads'][Ad::TYPE_SIDEBAR] = Ad::where('position', Ad::TYPE_SIDEBAR)->get();
        return response(view('errors.403', $this->data));
    }

    public function show404()
    {
        $this->data['ads'][Ad::TYPE_SIDEBAR] = Ad::where('position', Ad::TYPE_SIDEBAR)->get();
        return response(view('errors.404', $this->data), 404);
    }

    public function show500()
    {
        $this->data['ads'][Ad::TYPE_SIDEBAR] = Ad::where('position', Ad::TYPE_SIDEBAR)->get();
        return response(view('errors.500', $this->data), 500);
    }

    public function show503()
    {
        $this->data['ads'][Ad::TYPE_SIDEBAR] = Ad::where('position', Ad::TYPE_SIDEBAR)->get();
        return response(view('errors.503', $this->data), 503);
    }
}
