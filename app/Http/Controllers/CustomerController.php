<?php

namespace App\Http\Controllers;

use App\GalleryImage;
use App\Libraries\Parser;
use App\Libraries\Utils;
use App\Posts;
use App\PostTags;
use App\Sources;
use App\Tags;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Input;
use Sabre\Xml\LibXMLException;
use Session;
use Stripe\Charge;
use Stripe\Stripe;

class CustomerController extends Controller
{
    public function posts()
    {
        $posts = \DB::table('posts')->orderBy('id', 'asc');
        $payments = \DB::table('payments')->first();

        $search = Input::get('search', null);


        if ($search != null) {

            $posts = $posts->where('title', 'LIKE', '%' . $search . '%')->where('user_id', \Auth::user()->id)->paginate(10);

        } else {


            $posts = $posts->where('user_id', \Auth::user()->id)->paginate(10);


        }


        foreach ($posts as $post) {

            $post->category = SubCategory::where('id', $post->category_id)->first();

            if ($post->type == Post::TYPE_SOURCE) {
                $post->source = Source::where('id', $post->source_id)->first();
            }
        }

        return view('customer.posts.all', ['posts' => $posts , 'payments' => $payments]);

    }


    public function sources()
    {

        $sources = Source::where('user_id', \Auth::user()->id)->get();

        $payments = \DB::table('payments')->first();


        foreach ($sources as $source) {
            $source->category = SubCategory::where('id', $source->category_id)->first();
            $source->parent_category = Category::where('id', $source->category->parent_id)->first();
            $source->post = Post::where('source_id', $source->id)->first();
        }

        return view('customer.sources.all', ['sources' => $sources, 'payments' => $payments]);

    }

    public function createSource()
    {
        return view('customer.sources.create', ['categories' => Category::all()]);
    }

    public function storeSource()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('url')) {
            Session::flash('error_msg', trans('messages.feed_url_required'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!Input::has('sub_category')) {
            Session::flash('error_msg', trans('messages.sub_category_required'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!filter_var(Input::get('url'), FILTER_VALIDATE_URL)) {
            Session::flash('error_msg', trans('messages.invalid_url_should_start_with'));
            return redirect()->back()->withInput(Input::all());
        }

        $url = Input::get('url');

        try {
            $feed = Parser::xml($url);
        } catch (LibXMLException $e) {
            Session::flash('error_msg', trans('messages.invalid_source_url_only_rss_or_atom_allowed'));
            return redirect()->back()->withInput(Input::all());
        }

        $source = new Sources();
        $source->url = Input::get('url');
        $source->priority = 1;
        $source->category_id = Input::get('sub_category');
        $source->user_id = \Auth::user()->id;
        $source->channel_title = isset($feed['channel']['title']) ? $feed['channel']['title'] : '';
        $source->channel_link = isset($feed['channel']['link']) ? $feed['channel']['link'] : '';
        $source->channel_description = isset($feed['channel']['description']) ? $feed['channel']['description'] : '';
        $source->channel_language = isset($feed['channel']['language']) ? $feed['channel']['language'] : '';
        $source->channel_pubDate = isset($feed['channel']['pubDate']) ? $feed['channel']['pubDate'] : '';
        $source->channel_lastBuildDate = isset($feed['channel']['lastBuildDate']) ? $feed['channel']['lastBuildDate'] : '';
        $source->channel_generator = isset($feed['channel']['generator']) ? $feed['channel']['generator'] : '';
        $source->auto_update = Input::has('auto_update') ? 1 : 0;
        $source->items_count = isset($feed['channel']['item']) ? sizeof($feed['channel']['item']) : 0;
        $source->dont_show_author_publisher = Input::has('dont_show_author_publisher') ? 1 : 0;
        $source->show_post_source = Input::has('show_post_source') ? 1 : 0;
        $source->fetch_full_text = Input::has('fetch_full_text') ? 1 : 0;
        $source->use_auto_spin = Input::has('use_auto_spin') ? 1 : 0;
        $source->save();




        if (isset($feed['channel']['item'])) {
            foreach ($feed['channel']['item'] as $item) {

                if (!is_null($item['title']) && !is_null($item['description']) && !is_null($item['pubDate']) && !is_null($item['link'])) {

                    if ($source->fetch_full_text == 1) {
                        $item['description'] = Parser::fetchFull($item['link'], $item['description']);
                    }

                    if ($source->use_auto_spin == 1) {
                        $arr = Parser::spin($item['title'], $item['description']);
                        $item['title'] = $arr['title'];
                        $item['description'] = $arr['description'];
                    }

                    //If same title not present then only insert
                    if (sizeof(Post::where('slug', Str::slug($item['title']))->get()) <= 0) {

                        list($item['render_type'], $item['featured_image']) = Parser::setImgAndRenderType($item['description'], $item['video_embed_code'], $item['featured_image']);

                        $post_item = new Posts();
                        $post_item->author_id = \Auth::user()->id;
                        $post_item->title = $item['title'];
                        $post_item->save();

                        $post_item->slug = (is_null(Str::slug($item['title'])) || empty(Str::slug($item['title']))) ? $post_item->id : Str::slug($item['title']);
                        $post_item->link = $item['link'];
                        $post_item->featured = 0;
                        $post_item->category_id = Input::get('sub_category');
                        $post_item->type = Post::TYPE_SOURCE;
                        $post_item->render_type = $item['render_type'];
                        $post_item->source_id = $source->id;
                        $post_item->description = $item['description'];
                        $post_item->featured_image = $item['featured_image'];
                        $post_item->video_embed_code = $item['video_embed_code'];
                        $post_item->rating_box = 0;
                        $post_item->status = Input::get('status');
                        $post_item->created_at = $item['pubDate'];
                        $post_item->dont_show_author_publisher = Input::has('dont_show_author_publisher') ? 1 : 0;
                        $post_item->show_post_source = Input::has('show_post_source') ? 1 : 0;
                        $post_item->show_author_box = Input::has('dont_show_author_publisher') ? 0 : 1;
                        $post_item->show_author_socials = Input::has('dont_show_author_publisher') ? 0 : 1;
                        $post_item->views = 1;
                        $post_item->save();


                        if (isset($item['categories'])) {
                            $this->createTags($item['categories'], $post_item->id);
                        }
                    }
                }
            }
        }

        Session::flash('success_msg', trans('messages.source_created_success'));
        return redirect()->to('/customer/sources');
    }

    public function deleteSource($id)
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!is_null($id) && sizeof(Source::where('id', $id)->get()) > 0) {

            Post::where('source_id', $id)->delete();
            Source::where('id', $id)->delete();

            Session::flash('success_msg', trans('messages.source_deleted_success'));
            return redirect()->to('/customer/sources');

        } else {
            Session::flash('error_msg', trans('messages.source_not_found'));
            return redirect()->to('/customer/sources');
        }
    }

    public function createTags($tags, $post_id): void
    {
        $post_tags = PostTag::where('post_id', $post_id)->get();

        foreach ($post_tags as $post_tag) {
            Tags::where('id', $post_tag->tag_id)->delete();
        }

        PostTag::where('post_id', $post_id)->delete();


        foreach ($tags as $tag) {

            $old_tag = Tags::where('title', $tag)->first();

            if (sizeof($old_tag) > 0) {

                $pt = new PostTags();
                $pt->post_id = $post_id;
                $pt->tag_id = $old_tag->id;
                $pt->save();

            } else {
                $new_tag = new Tags();
                $new_tag->title = $tag;
                $new_tag->slug = Str::slug($tag);
                $new_tag->save();

                $pt = new PostTags();
                $pt->post_id = $post_id;
                $pt->tag_id = $new_tag->id;
                $pt->save();
            }
        }


    }



    public function stripe($id)
    {
        $token = Input::get('stripeToken');

        $payments = \DB::table('payments')->first();

        Stripe::setApiKey($payments->stripe_api_secret);
        Charge::create(array(
             'amount' => ($payments->rate_per_post) * 100,
             'currency' => 'usd',
             'source' => $token
         ));

        Post::where('id', $id)->update(['status' => 1]);

        \DB::table("recent_payments")->insert(['email' => \Auth::user()->email , 'amount' => $payments->rate_per_post , 'date' => Carbon::today() , 'Reason' => "Adding Post"]);

        return redirect()->back();
    }

    public function stripeSources($id)
    {
        $token = Input::get('stripeToken');

        $payments = \DB::table('payments')->first();

        Stripe::setApiKey($payments->stripe_api_secret);
        Charge::create(array(
            'amount' => ($payments->rate_per_source) * 100,
            'currency' => 'usd',
            'source' => $token
        ));

        Post::where('source_id', $id)->update(['status' => 1]);

        \DB::table("recent_payments")->insert(['email' => \Auth::user()->email , 'amount' => $payments->rate_per_source , 'date' => Carbon::today() , 'Reason' => "Adding Source"]);


        return redirect()->back();
    }

    public function payments()
    {
        $payments = \DB::table('recent_payments')->where('email', \Auth::user()->email)->get();


        return view('customer.payments', ['payments' => $payments]);


    }



    public function create()
    {
        $admins = Utils::getUsersInGroup(User::TYPE_ADMIN);

        return view('customer.posts.create', ['categories' => Category::all(), 'admins' => $admins]);
    }

    public function store()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        $v = \Validator::make(['title' => Input::get('title'),
            'description' => Input::get('description'),
            'category_id' => Input::get('sub_category')
        ], ['title' => 'required', 'description' => 'required', 'category_id' => 'required']);

        if ($v->fails()) {
            Session::flash('error_msg', Utils::messages($v));
            return redirect()->back()->withInput(Input::all());
        }

        $title = Input::get('title');
        $description = Input::get('description');

        if (Input::has('spin')) {
            $arr = Parser::spin(Input::get('title'), Input::get('description'));
            $title = $arr['title'];
            $description = $arr['description'];
        }

        $post_item = new Posts();
        $post_item->author_id = \Auth::user()->id;
        $post_item->user_id = \Auth::user()->id;
        $post_item->title = $title;
        $post_item->slug = Str::slug($title);
        $post_item->featured = Input::has('featured');
        $post_item->category_id = Input::get('sub_category');
        $post_item->description = $description;
        $post_item->type = Post::TYPE_MANUAL;
        $post_item->render_type = Input::get('render_type');
        $post_item->video_embed_code = Input::get('video_embed_code');
        $post_item->image_parallax = Input::has('image_parallax');
        $post_item->video_parallax = Input::has('video_parallax');
        $post_item->rating_box = Input::get('rating_box');
        $post_item->rating_desc = Input::get('rating_desc');
        $post_item->show_in_mega_menu = Input::has('show_in_mega_menu');
        $post_item->show_featured_image_in_post = Input::has('show_featured_image_in_post');
        $post_item->show_author_box = Input::has('show_author_box');
        $post_item->show_author_socials = Input::has('show_author_socials');
        $post_item->views = 0;

        if (Input::hasFile('featured_image')) {
            $post_item->featured_image = Utils::imageUpload(Input::file('featured_image'), 'images');
        }

        $post_item->status = Input::get('status');
        $post_item->save();

        if (strlen(Input::get('tags')) > 0) {

            $tags = explode(',', Input::get('tags'));

            foreach ($tags as $tag) {

                $old_tag = Tags::where('title', $tag)->first();

                if (sizeof($old_tag) > 0) {

                    $pt = new PostTags();
                    $pt->post_id = $post_item->id;
                    $pt->tag_id = $old_tag->id;
                    $pt->save();

                } else {
                    $new_tag = new Tags();
                    $new_tag->title = $tag;
                    $new_tag->slug = Str::slug($tag);
                    $new_tag->save();

                    $pt = new PostTags();
                    $pt->post_id = $post_item->id;
                    $pt->tag_id = $new_tag->id;
                    $pt->save();
                }

            }
        }

        if (Input::hasFile('image_gallery') && Input::get('render_type') == Post::RENDER_TYPE_GALLERY) {

            $gallery = Input::file('image_gallery');

            foreach ($gallery as $index => $g) {
                $file = Utils::imageUpload($g, 'images');
                $i = new GalleryImage();
                $i->post_id = $post_item->id;
                $i->image = $file;
                $i->save();

                if ($index == 0) {
                    $featured = $file;
                }
            }

            if (sizeof($gallery) > 0) {
                $post_item->featured_image = $featured;
                $post_item->save();
            }


        }


        Session::flash('success_msg', trans('messages.post_created_success'));
        return redirect()->to('/customer');
    }

    public function edit($id)
    {

        if (!is_null($id) && sizeof(Post::where('id', $id)->get()) > 0) {

            $post = Post::where('id', $id)->first();

            $post->category = SubCategory::where('id', $post->category_id)->first();
            $post->parent_category = Category::where('id', $post->category->parent_id)->first();

            $post->gallery = GalleryImage::where('post_id', $post->id)->get();

            $post_tags = PostTag::where('post_id', $post->id)->pluck('tag_id');

            if (sizeof($post_tags) > 0) {
                $post->tags = Tags::whereIn('id', $post_tags)->pluck('title')->toArray();
            } else {
                $post->tags = [];
            }

            if ($post->type == Post::TYPE_SOURCE) {
                $post->source = Source::where('id', $post->source_id)->first();
            }

            $admins = Utils::getUsersInGroup(User::TYPE_ADMIN);

            return view('customer.posts.edit', ['post' => $post, 'categories' => Category::all(), 'sub_categories' => SubCategory::where('parent_id', $post->parent_category->id)->get(), 'admins' => $admins]);

        } else {
            Session::flash('error_msg', trans('messages.post_not_found'));
            return redirect()->to('/customer');
        }

    }


    public function update()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(Post::where('id', Input::get('id'))->get()) > 0) {

            if (sizeof(Post::where('title', Input::get('title'))->where('id', '!=', Input::get('id'))->get()) > 0) {
                Session::flash('error_msg', trans('messages.post_with_title_exists'));
                return redirect()->back()->withInput(Input::except("featured_image"));
            }

            $post_item = Post::where('id', Input::get('id'))->first();
            $post_item->author_id = Input::get('author');
            $post_item->user_id = \Auth::user()->id;
            $post_item->title = Input::get('title');
            $post_item->slug = Str::slug(Input::get('title'));
            $post_item->featured = Input::has('featured');
            $post_item->category_id = Input::get('sub_category');
            $post_item->description = Input::get('description');
            $post_item->render_type = Input::get('render_type');
            $post_item->video_embed_code = Input::get('video_embed_code');
            $post_item->image_parallax = Input::has('image_parallax');
            $post_item->video_parallax = Input::has('video_parallax');
            $post_item->rating_box = Input::get('rating_box');
            $post_item->rating_desc = Input::get('rating_desc');
            $post_item->show_in_mega_menu = Input::has('show_in_mega_menu');
            $post_item->show_featured_image_in_post = Input::has('show_featured_image_in_post');
            $post_item->show_author_box = (Input::has('dont_show_author_publisher') ? 0 : (Input::has('show_author_box') ? 1 : 0));
            $post_item->show_author_socials = (Input::has('dont_show_author_publisher') ? 0 : (Input::has('show_author_socials') ? 1 : 0));

            $post_item->dont_show_author_publisher = Input::has('dont_show_author_publisher') ? 1 : 0;
            $post_item->show_post_source = Input::has('show_post_source') ? 1 : 0;

            if (Input::hasFile('featured_image')) {
                $post_item->featured_image = Utils::imageUpload(Input::file('featured_image'), 'images');
                ;
            }

            $post_item->status = Input::get('status');
            $post_item->save();

            PostTag::where('post_id', $post_item->id)->delete();

            if (strlen(Input::get('tags')) > 0) {

                $tags = explode(',', Input::get('tags'));

                foreach ($tags as $tag) {

                    $old_tag = Tags::where('title', $tag)->first();

                    if (sizeof($old_tag) > 0) {

                        $pt = new PostTags();
                        $pt->post_id = $post_item->id;
                        $pt->tag_id = $old_tag->id;
                        $pt->save();

                    } else {
                        $new_tag = new Tags();
                        $new_tag->title = $tag;
                        $new_tag->slug = Str::slug($tag);
                        $new_tag->save();

                        $pt = new PostTags();
                        $pt->post_id = $post_item->id;
                        $pt->tag_id = $new_tag->id;
                        $pt->save();
                    }
                }
            }

            if (Input::hasFile('image_gallery') && Input::get('render_type') == Post::RENDER_TYPE_GALLERY) {

                GalleryImage::where('post_id', $post_item->id)->delete();

                $gallery = Input::file('image_gallery');

                foreach ($gallery as $g) {
                    $file = Utils::imageUpload($g, 'images');
                    $i = new GalleryImage();
                    $i->post_id = $post_item->id;
                    $i->image = $file;
                    $i->save();
                }
            }

            $post_gallery_img = GalleryImage::where('post_id', $post_item->id)->first();

            if (!empty($post_gallery_img) && Input::get('render_type') == Post::RENDER_TYPE_GALLERY) {
                $post_item->featured_image = $post_gallery_img->image;
            }

            $post_item->save();

            Session::flash('success_msg', trans('messages.post_updated_success'));
            return redirect()->to('/customer');

        } else {
            Session::flash('error_msg', trans('messages.post_not_found'));
            return redirect()->to('/customer');
        }

    }

    public function delete($id)
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (!is_null($id) && sizeof(Post::where('id', $id)->get()) > 0) {

            Post::where('id', $id)->delete();

            $post_tags = PostTag::where('post_id', $id)->get();

            foreach ($post_tags as $post_tag) {
                Tags::where('id', $post_tag->tag_id)->delete();
            }

            PostTag::where('post_id', $id)->delete();


            Session::flash('success_msg', trans('messages.post_deleted_success'));
            return redirect()->to('/customer');

        } else {
            Session::flash('error_msg', trans('messages.post_not_found'));
            return redirect()->to('/customer');
        }
    }

    public function profileEdit()
    {

        $id = \Auth::user()->id;


        $user = User::where('id', $id)->first();
        $countries = \DB::table('countries')->get();


        return view('customer.profile', ['user' => $user, 'countries' => $countries]);

    }

    public function Profileupdate()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        if (Input::has('id') && sizeof(User::where('id', Input::get('id'))->get()) > 0) {

            if (sizeof(User::where('email', Input::get('email'))->where('id', '!=', Input::get('id'))->get()) > 0) {
                Session::flash('error_msg', 'Email already exists');
                return redirect()->back()->withInput(Input::all());
            }

            if (sizeof(User::where('name', Input::get('name'))->where('id', '!=', Input::get('id'))->get()) > 0) {
                Session::flash('error_msg', 'Name already exists');
                return redirect()->back()->withInput(Input::all());
            }

            $data = [
                'name' => Input::get('name'),
                'email' => Input::get('email')
            ];

            $rules = [
                'name' => 'required',
                'email' => 'required|email'
            ];

            if (strlen(Input::get('password')) > 0) {
                $data['password'] = Input::get('password');
                $data['password_confirmation'] = Input::get('password_confirmation');

                $rules['password'] = 'required|confirmed';
                $rules['password_confirmation'] = 'required';
            }

            $v = \Validator::make($data, $rules);

            if ($v->fails()) {
                Session::flash('error_msg', Utils::messages($v));
                return redirect()->back()->withInput(Input::except('avatar'));
            }

            $user = User::where('id', Input::get('id'))->first();

            $user->name = Input::get('name');
            $user->email = Input::get('email');

            if (strlen(Input::get('password')) > 0) {
                $user->password = \Hash::make(Input::get('password'));
            }

            $user->avatar = Input::hasFile('avatar') ? Utils::imageUpload(Input::file('avatar')) : Input::get('old_avatar');
            $user->birthday = Input::get('dob');
            $user->bio = Input::get('bio');
            $user->gender = Input::get('gender');
            $user->mobile_no = Input::get('mobile_no');
            $user->fb_url = Input::get('fb_url');
            $user->fb_page_url = Input::get('fb_page_url');
            $user->website_url = Input::get('website_url');
            $user->twitter_url = Input::get('twitter_url');
            $user->google_plus_url = Input::get('google_plus_url');
            $user->country = Input::get('country');
            $user->activated = Input::has('activate');



            $user->save();


            Session::flash('success_msg', trans('messages.user_updated_success'));
            return redirect()->to('/customer/profile');
        }

    }





}
