<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Models\Post;
use App\Models\Source;
use App\Models\User;
use File;
use Illuminate\Support\Facades\Artisan;
use Input;
use Session;
use SplFileInfo;
use URL;

class DashboardController extends Controller
{
    public function updateApplication()
    {
        Artisan::call('cache:clear');
        Artisan::call('migrate', ['--force' => 'yes']);

        Session::flash('success_msg', 'Application successfully updated');

        return redirect()->back();
    }

    public function giveMeWriteAccess(): string
    {
        Session::put('GIVE-ME-WRITE-ACCESS', true);
        return 'Done dana done now u have write access';
    }

    public function removeWriteAccess(): string
    {
        Session::forget('GIVE-ME-WRITE-ACCESS');
        return 'Nice to meet you , see you later ba bye';
    }

    public function index()
    {
        $posts_count = Post::count();
        $sources_count = Source::count();
        $users_count = User::count();

        return view('admin.index', ['posts_count' => $posts_count, 'sources_count' => $sources_count, 'users_count' => $users_count]);
    }

    public function handleRedactorUploads()
    {
        $filename = Utils::imageUpload(Input::file('file'), 'images');
        return response()->json($data = array(
            'filelink' => $filename
        ), 200);
    }

    /**
     * @return array<mixed, array<'image'|'thumb'|'title', mixed>>
     */
    public function redactorImages(): array
    {

        $arr = [];
        $allFiles = File::allFiles(public_path() . '/uploads/images/');

        foreach ($allFiles as $file) {
            $file = new SplFileInfo($file);
            $arr[] = ["thumb" => URL::to('/') . '/uploads/images/' . $file->getFilename(), "image" => URL::to('/') . '/uploads/images/' . $file->getFilename(), "title" => $file->getFilename()];
        }

        return $arr;
    }

}
