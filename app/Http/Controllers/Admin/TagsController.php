<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PostTag;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index()
    {
        $tags = Tag::orderBy('id', 'asc')->paginate(10);

        foreach ($tags as $tag) {
            $tag->post_count = PostTag::where('tag_id', $tag->id)->count();
        }

        return view('admin.tags.all', compact('tags'));
    }

    public function destroy(Tag $tag)
    {
        $tag->delete();
        return redirect()->route('tags.index')->with('success_msg', trans('messages.tag_deleted_success'));
    }

    public function multiDelete(Request $request)
    {
        $tagIds = $request->all()['ids'];

        PostTag::whereIn('tag_id', $tagIds)->delete();
        Tag::whereIn('id', $tagIds)->delete();

        return redirect()->route('tags.index')->with('success_msg', trans('messages.tags_deleted_success'));
    }
}
