<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Libraries\Parser;
use App\Libraries\Utils;
use App\Models\Category;
use App\Models\Post;
use App\Models\PostTag;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class PostsController extends Controller
{
    public function create()
    {
        $admins = Utils::getUsersInGroup(User::TYPE_ADMIN);
        return view('admin.posts.create', [
            'categories' => Category::all(),
            'admins' => $admins
        ]);
    }

    public function store(StorePostRequest $request)
    {
        $validated = $request->validated();

        $title = $validated['title'];
        $description = $validated['description'];

        if ($request->has('spin')) {
            $arr = Parser::spin($title, $description);
            $title = $arr['title'];
        }

        $post = new Post();
        $post->fill($validated);
        $post->slug = Str::slug($title);
        $post->featured = $request->has('featured');
        $post->views = 0;

        if ($request->hasFile('featured_image')) {
            $post->featured_image = Utils::imageUpload($request->file('featured_image'), 'images');
        }

        $post->save();

        // Process tags
        if (!empty($validated['tags'])) {
            $tags = explode(',', $validated['tags']);
            foreach ($tags as $tag) {
                $tagModel = Tag::firstOrCreate(['title' => $tag], ['slug' => Str::slug($tag)]);
                PostTag::create(['post_id' => $post->id, 'tag_id' => $tagModel->id]);
            }
        }

        Session::flash('success_msg', trans('messages.post_created_success'));
        return redirect()->route('posts.index');
    }

    public function edit(Post $post)
    {
        $admins = Utils::getUsersInGroup(User::TYPE_ADMIN);

        return view('admin.posts.edit', [
            'post' => $post->with(['category', 'tags']),
            'categories' => Category::all(),
            'admins' => $admins
        ]);
    }

    public function update(UpdatePostRequest $request, Post $post)
    {
        $validated = $request->validated();
        $post->fill($validated);
        $post->slug = Str::slug($validated['title']);
        $post->featured = $request->has('featured');

        if ($request->hasFile('featured_image')) {
            $post->featured_image = Utils::imageUpload($request->file('featured_image'), 'images');
        }

        $post->save();

        // Update tags
        PostTag::where('post_id', $post->id)->delete();
        if (!empty($validated['tags'])) {
            $tags = explode(',', $validated['tags']);
            foreach ($tags as $tag) {
                $tagModel = Tag::firstOrCreate(['title' => $tag], ['slug' => Str::slug($tag)]);
                PostTag::create(['post_id' => $post->id, 'tag_id' => $tagModel->id]);
            }
        }

        Session::flash('success_msg', trans('messages.post_updated_success'));
        return redirect()->route('posts.index');
    }

    public function index(Request $request)
    {
        $query = Post::query();

        if ($request->filled('search')) {
            $query->where('title', 'LIKE', '%' . $request->input('search') . '%');
        }
        $posts = $query->orderBy('id', 'asc')->paginate(10);

        return view('admin.posts.all', compact('posts'));
    }

    public function delete(Post $post)
    {
        $post->delete();
        PostTag::where('post_id', $post->id)->delete();
        Session::flash('success_msg', trans('messages.post_deleted_success'));
        return redirect()->route('posts.index');
    }

    public function multiDelete(Request $request)
    {
        $ids = $request->input('id', []);
        Post::whereIn('id', $ids)->delete();
        PostTag::whereIn('post_id', $ids)->delete();

        Session::flash('success_msg', trans('messages.posts_deleted_success'));
        return redirect()->route('posts.index');
    }
}
