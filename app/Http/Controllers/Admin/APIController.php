<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tag;

class APIController extends Controller
{
    public function getTags()
    {
        return Tag::pluck('title');
    }

}
