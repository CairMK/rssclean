<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostRating;
use Session;

class RatingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('has_permission:ratings.view', ['only' => ['all']]);
        $this->middleware('has_permission:ratings.delete', ['only' => ['delete']]);
    }

    public function all()
    {

        $ratings = PostRating::all();

        foreach ($ratings as $r) {
            $r->post = Post::where('id', $r->post_id)->first();
        }

        return view('admin.ratings.all', ['ratings' => $ratings]);
    }

    public function delete($id)
    {
        PostRating::findOrFail($id)->delete();

        Session::flash('success_msg', trans('messages.rating_delete_success'));

        return redirect()->route('ratings.all');
    }

}
