<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAdRequest;
use App\Http\Requests\UpdateAdRequest;
use App\Models\Ad;
use Session;

class AdsController extends Controller
{
    public function create()
    {

        return view('admin.ads.create');
    }

    public function store(StoreAdRequest $request)
    {
        Ad::create($request->validated());

        Session::flash('success_msg', trans('messages.ads_created_success'));
        return redirect()->route('ads.index');

    }

    public function edit(Ad $ad)
    {

        return view('admin.ads.edit', ['ad' => $ad]);
    }


    public function update(UpdateAdRequest $request, Ad $ad)
    {
        $ad->update($request->validated());

        Session::flash('success_msg', trans('messages.ad_updated_success'));
        return redirect()->route('ads.index');


    }

    public function index()
    {
        return view('admin.ads.all', ['ads' => Ad::all()]);
    }


    public function delete(Ad $ad)
    {
        $ad->delete();
        Session::flash('success_msg', trans('messages.ad_deleted_success'));

        return redirect()->route('ads.index');
    }

}
