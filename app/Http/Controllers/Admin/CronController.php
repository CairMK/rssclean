<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CronJob;
use Illuminate\Support\Facades\Artisan;
use Session;

class CronController extends Controller
{
    public function index()
    {
        $crons = CronJob::orderby('created_at', 'desc')->get();
        return view('admin.crons.all', ['crons' => $crons]);
    }

    public function run()
    {
        Artisan::call('update-sources');
        Session::flash('success_msg', trans('messages.cron_run_success'));
        return redirect()->back();
    }

    public function view(CronJob $cronJob)
    {
        return view('admin.crons.view', ['cron' => $cronJob]);
    }


    public function delete(CronJob $cronJob)
    {

        $cronJob->delete();
        Session::flash('success_msg', trans('messages.cron_deleted_success'));
        return redirect()->route('crons.index');


    }

}
