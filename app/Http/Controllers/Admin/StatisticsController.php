<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class StatisticsController extends Controller
{
    public function __construct()
    {
        $this->middleware('has_permission:statistics.view', ['only' => ['all']]);
    }

    public function all()
    {
        return view('admin.statistics.all');
    }

}
