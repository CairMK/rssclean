<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Libraries\Utils;
use App\Models\Category;
use App\Models\Page;
use App\Models\User;
use Illuminate\Support\Str;
use Input;
use Session;

class PagesController extends Controller
{

    public function create()
    {
        $admins = Utils::getUsersInGroup(User::TYPE_ADMIN);

        return view('admin.pages.create', ['categories' => Category::all(), 'admins' => $admins]);
    }

    public function store(StorePageRequest $request)
    {

        Page::create($request->validated());

        Session::flash('success_msg', trans('messages.page_created_success'));
            return redirect()->route('pages.index');

    }

    public function edit(Page $page)
    {

        $admins = Utils::getUsersInGroup(User::TYPE_ADMIN);


            return view('admin.pages.edit', ['page' => $page,'admins' => $admins]);


    }

    public function update(UpdatePageRequest $request, Page $page)
    {

        $page->update($request->validated());

            Session::flash('success_msg', trans('messages.page_updated_success'));
            return redirect()->route('pages.index');

        
    }

    public function index()
    {

        $pages = Page::all();

        return view('admin.pages.all', ['pages' => $pages]);
    }

    public function delete(Page $page)
    {
        $page->delete();

            Session::flash('success_msg', trans('messages.page_deleted_success'));
            return redirect()->route('pages.index');


    }

}
