<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSourceRequest;
use App\Http\Requests\UpdateSourceRequest;
use App\Libraries\Parser;
use App\Libraries\Utils;
use App\Models\Category;
use App\Models\Post;
use App\Models\PostTag;
use App\Models\Source;
use App\Models\Tag;
use Cohensive\OEmbed\Exceptions\ExtractorException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Sabre\Xml\LibXMLException;

class SourcesController extends Controller
{
    public function index()
    {
        $sources = Source::all();
        return view('admin.sources.all', compact('sources'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.sources.create', compact('categories'));
    }

    public function store(StoreSourceRequest $request)
    {
        try {
            $feed = Parser::xml($request->url);
        } catch (LibXMLException $e) {
            return redirect()->back()->withInput()->withErrors(['url' => trans('messages.invalid_source_url_only_rss_or_atom_allowed')]);
        } catch (ExtractorException $e) {
        }

        $source = Source::create([
            'url' => $request->url,
            'priority' => $request->priority,
            'category_id' => $request->category_id,
            'channel_title' => $feed['channel']['title'] ?? '',
            'channel_link' => $feed['channel']['link'] ?? '',
            'channel_description' => $feed['channel']['description'] ?? '',
            'channel_language' => $feed['channel']['language'] ?? '',
            'channel_pubDate' => $feed['channel']['pubDate'] ?? '',
            'channel_lastBuildDate' => $feed['channel']['lastBuildDate'] ?? '',
            'channel_generator' => $feed['channel']['generator'] ?? '',
            'auto_update' => $request->boolean('auto_update'),
            'show_post_source' => $request->boolean('show_post_source'),
            'fetch_full_text' => $request->boolean('fetch_full_text'),
            'use_auto_spin' => $request->boolean('use_auto_spin'),
            'dont_show_author_publisher' => $request->boolean('dont_show_author_publisher'),
            'items_count' => isset($feed['channel']['item']) ? count($feed['channel']['item']) : 0,
        ]);

        if (isset($feed['channel']['item'])) {
            foreach ($feed['channel']['item'] as $item) {
                $this->storePostFromFeed($item, $source);
            }
        }

        return redirect()->route('sources.index')->with('success_msg', trans('messages.source_created_success'));
    }

    public function edit(Source $source)
    {
        $categories = Category::all();
        return view('admin.sources.edit', compact('source', 'categories'));
    }

    public function update(UpdateSourceRequest $request, Source $source)
    {
        $source->update([
            'url' => $request->url,
            'priority' => $request->priority,
            'category_id' => $request->category_id,
            'auto_update' => $request->boolean('auto_update'),
            'show_post_source' => $request->boolean('show_post_source'),
            'fetch_full_text' => $request->boolean('fetch_full_text'),
            'use_auto_spin' => $request->boolean('use_auto_spin'),
            'dont_show_author_publisher' => $request->boolean('dont_show_author_publisher'),
        ]);

        Post::where('source_id', $source->id)->update([
            'category_id' => $source->category_id,
            'dont_show_author_publisher' => $source->dont_show_author_publisher,
            'show_post_source' => $source->show_post_source,
        ]);

        return redirect()->route('sources.index')->with('success_msg', trans('messages.source_updated_success'));
    }

    public function delete(Source $source)
    {
        Post::where('source_id', $source->id)->delete();
        $source->delete();
        return redirect()->route('sources.index')->with('success_msg', trans('messages.source_deleted_success'));
    }

    public function refresh(Source $source)
    {
        if (!Utils::hasWriteAccess()) {
            return redirect()->back()->with('error_msg', trans('messages.preview_mode_error'));
        }

        try {
            $feed = Parser::xml($source->url);
        } catch (LibXMLException $e) {
            return redirect()->back()->with('error_msg', trans('messages.invalid_source_url_only_rss_or_atom_allowed'));
        } catch (ExtractorException $e) {
        }

        if (isset($feed['channel']['item'])) {
            foreach ($feed['channel']['item'] as $item) {
                $this->storePostFromFeed($item, $source);
            }
        }

        $source->save();
        return redirect()->route('sources.index')->with('success_msg', trans('messages.source_updated_success'));
    }

    private function storePostFromFeed(array $item, Source $source)
    {
        if (empty($item['title']) || empty($item['description']) || empty($item['pubDate']) || empty($item['link'])) {
            return;
        }

        if ($source->fetch_full_text) {
            $item['description'] = Parser::fetchFull($item['link'], $item['description']);
        }

        if ($source->use_auto_spin) {
            $arr = Parser::spin($item['title'], $item['description']);
            $item['title'] = $arr['title'];
            $item['description'] = $arr['description'];
        }

        [$item['render_type'], $item['featured_image']] = Parser::setImgAndRenderType($item['description'], $item['video_embed_code'], $item['featured_image']);

        if (Post::where('slug', Str::slug($item['title']))->doesntExist()) {
            $post = Post::create([
                'author_id' => Auth::id(),
                'title' => $item['title'],
                'slug' => Str::slug($item['title']) ?: Str::random(10),
                'link' => $item['link'],
                'category_id' => $source->category_id,
                'type' => Post::TYPE_SOURCE,
                'render_type' => $item['render_type'],
                'source_id' => $source->id,
                'description' => $item['description'],
                'featured_image' => $item['featured_image'],
                'video_embed_code' => $item['video_embed_code'],
                'dont_show_author_publisher' => $source->dont_show_author_publisher,
                'show_post_source' => $source->show_post_source,
                'show_author_box' => !$source->dont_show_author_publisher,
                'show_author_socials' => !$source->dont_show_author_publisher,
                'status' => Post::STATUS_PUBLISHED,
                'created_at' => $item['pubDate'],
                'views' => 1,
            ]);

            if (!empty($item['categories'])) {
                $this->createTags($item['categories'], $post->id);
            }
        }
    }

    private function createTags($tags, $post_id)
    {
        PostTag::where('post_id', $post_id)->delete();

        foreach ($tags as $tag) {
            $existingTag = Tag::firstOrCreate(['title' => $tag], ['slug' => Str::slug($tag)]);
            PostTag::create(['post_id' => $post_id, 'tag_id' => $existingTag->id]);
        }
    }
}
