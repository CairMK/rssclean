<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Utils;
use App\Models\Post;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Input;
use Session;

class SettingsController extends Controller
{

    public function all()
    {

        $data = [];

        $data[Setting::CATEGORY_GENERAL] = Utils::getSettings(Setting::CATEGORY_GENERAL);
        $data[Setting::CATEGORY_SEO] = Utils::getSettings(Setting::CATEGORY_SEO);
        $data[Setting::CATEGORY_COMMENTS] = Utils::getSettings(Setting::CATEGORY_COMMENTS);
        $data[Setting::CATEGORY_SOCIAL] = Utils::getSettings(Setting::CATEGORY_SOCIAL);
        $data[Setting::CATEGORY_OLD_NEWS] = Utils::getSettings(Setting::CATEGORY_OLD_NEWS);
        $data[Setting::CATEGORY_CUSTOM_JS] = Utils::getSettings(Setting::CATEGORY_CUSTOM_JS);
        $data[Setting::CATEGORY_CUSTOM_CSS] = Utils::getSettings(Setting::CATEGORY_CUSTOM_CSS);
        $payments = DB::table('payments')->first();

        $data['timezones'] = DB::table('timezones')->get();
        $data['locales'] = DB::table('locales')->get();

        return view('admin.settings.all', $data, ['payments' => $payments]);
    }

    public function updateCustomCSS()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Setting::CATEGORY_CUSTOM_CSS,
            Setting::CATEGORY_CUSTOM_CSS,
            Input::get('custom_css'),
            Setting::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateCustomJS()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Setting::CATEGORY_CUSTOM_JS,
            Setting::CATEGORY_CUSTOM_JS,
            Input::get('custom_js'),
            Setting::TYPE_TEXT
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateSocial()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'fb_page_url',
            Input::get('fb_page_url'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'twitter_url',
            Input::get('twitter_url'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'twitter_handle',
            Input::get('twitter_handle'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'google_plus_page_url',
            Input::get('google_plus_page_url'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'skype_username',
            Input::get('skype_username'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'youtube_channel_url',
            Input::get('youtube_channel_url'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'vimeo_channel_url',
            Input::get('vimeo_channel_url'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'addthis_js',
            Input::get('addthis_js'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'sharethis_js',
            Input::get('sharethis_js'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'sharethis_span_tags',
            Input::get('sharethis_span_tags'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'facebook_box_js',
            Input::get('facebook_box_js'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'twitter_box_js',
            Input::get('twitter_box_js'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'show_sharing',
            Input::has('show_sharing') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SOCIAL,
            'show_big_sharing',
            Input::has('show_big_sharing') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateComments()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Setting::CATEGORY_COMMENTS,
            'comment_system',
            Input::get('comment_system'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_COMMENTS,
            'fb_js',
            Input::get('fb_js'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_COMMENTS,
            'fb_num_posts',
            Input::get('fb_num_posts'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_COMMENTS,
            'fb_comment_count',
            Input::has('fb_comment_count') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_COMMENTS,
            'disqus_js',
            Input::get('disqus_js'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_COMMENTS,
            'disqus_comment_count',
            Input::has('disqus_comment_count') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_COMMENTS,
            'show_comment_box',
            Input::has('show_comment_box') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateSEO()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SEO,
            'seo_keywords',
            Input::get('seo_keywords'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SEO,
            'seo_description',
            Input::get('seo_description'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SEO,
            'google_verify',
            Input::get('google_verify'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_SEO,
            'bing_verify',
            Input::get('bing_verify'),
            Setting::TYPE_STRING
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateGeneral()
    {

        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'site_url',
            Input::get('site_url'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'site_title',
            Input::get('site_title'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'analytics_code',
            Input::get('analytics_code'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'mailchimp_form',
            Input::get('mailchimp_form'),
            Setting::TYPE_TEXT
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'logo_76',
            Input::hasFile('logo_76') ? Utils::imageUpload(Input::file('logo_76'), 'images') : Input::get('logo_76_value'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'logo_120',
            Input::hasFile('logo_120') ? Utils::imageUpload(Input::file('logo_120'), 'images') : Input::get('logo_120_value'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'logo_152',
            Input::hasFile('logo_152') ? Utils::imageUpload(Input::file('logo_152'), 'images') : Input::get('logo_152_value'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'favicon',
            Input::hasFile('favicon') ? Utils::imageUpload(Input::file('favicon'), 'images') : Input::get('favicon_value'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'site_post_as_titles',
            Input::has('site_post_as_titles') ? 1 : 0,
            Setting::TYPE_CHECK
        );


        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'timezone',
            Input::get('timezone'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'locale',
            Input::get('locale'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'generate_sitemap',
            Input::has('generate_sitemap') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'generate_rss_feeds',
            Input::has('generate_rss_feeds') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_GENERAL,
            'include_sources',
            Input::has('include_sources') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function updateDeleteNews()
    {
        if (!Utils::hasWriteAccess()) {
            Session::flash('error_msg', trans('messages.preview_mode_error'));
            return redirect()->back()->withInput(Input::all());
        }

        Utils::setOrCreateSettings(
            Setting::CATEGORY_OLD_NEWS,
            'days',
            Input::get('delete_news_days'),
            Setting::TYPE_STRING
        );

        Utils::setOrCreateSettings(
            Setting::CATEGORY_OLD_NEWS,
            'auto_delete_old_news',
            Input::has('old_news_days_check') ? 1 : 0,
            Setting::TYPE_CHECK
        );

        Session::flash('success_msg', trans('messages.settings_updated_success'));
        return redirect()->to('/admin/settings');
    }

    public function deleteOldManually($days = null)
    {

        if (is_null($days)) {
            Session::flash('error_msg', trans('messages.please_enter_no_of_days'));
            return redirect()->to('/admin/settings');
        }

        $posts = Post::all();

        foreach ($posts as $post) {

            $created_at = $post->created_at;
            $now = Carbon::now();

            if ($now->diffInDays($created_at) >= $days) {
                $post->delete();
            }
        }

        Session::flash('success_msg', trans('messages.old_new_deleted_success'));
        return redirect()->to('/admin/settings');

    }

}
