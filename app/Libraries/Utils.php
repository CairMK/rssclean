<?php

namespace App\Libraries;

use App\Models\Group;
use App\Models\Setting;
use App\Models\User;
use App\Models\UsersGroup;
use Crypt;
use DB;
use Log;
use StdClass;
use URL;
use Session;

class Utils
{
    public static function getSettingsValue($category, $key, $type)
    {
        $setting = Setting::where('category', $category)->where('column_key', $key)->first();

        if (!empty($setting)) {
            if ($type == Setting::TYPE_STRING) {
                return $setting->value_string;
            }

            if ($type == Setting::TYPE_TEXT) {
                return $setting->value_txt;
            }

            if ($type == Setting::TYPE_CHECK) {
                return $setting->value_check;
            }
        }

        return "http://localhost";
    }

    /**
     * @param        $permissions
     * @param  bool  $only_keys
     * @return array
     */
    public static function parsePermissions($permissions, $only_keys = false): array
    {
        $actuals = [];
        $main_permissions = \Config::get('permissions');

        if (strlen($permissions) > 0) {

            $arr = explode(',', $permissions);

            foreach ($arr as $a) {

                foreach ($main_permissions as $m) {
                    foreach ($m['permissions'] as $permission) {
                        if ($permission['key'] == $a) {
                            if ($only_keys) {
                                $actuals[] = $permission['key'];
                            } else {
                                $actuals[] = $permission;
                            }
                        }
                    }
                }

            }

            return $actuals;

        } else {
            return [];
        }
    }

    public static function hasWriteAccess()
    {
        return Session::has('GIVE-ME-WRITE-ACCESS') ? true : env('WRITE_ACCESS');
    }

    public static function getUsersInGroup($group = User::TYPE_AUTHOR)
    {

        if (is_integer($group)) {
            $user_ids = UsersGroup::where("group_id", $group)->lists('user_id');
        } else {
            $find_group = Group::where("name", $group)->first();
            $user_ids = UsersGroup::where("group_id", $find_group->id)->pluck('user_id');
        }

        return sizeof($user_ids) > 0 ? User::whereIn('id', $user_ids)->get() : [];
    }

    public static function isAdmin($user_id)
    {
        return self::inGroup(User::TYPE_ADMIN, $user_id);
    }

    public static function isCustomer($user_id)
    {
        return self::inGroup(User::TYPE_CUSTOMER, $user_id);
    }

    public static function isPublisher($user_id)
    {
        return self::inGroup(User::TYPE_PUBLISHER, $user_id);
    }

    public static function isAuthor($user_id)
    {
        return self::inGroup(User::TYPE_AUTHOR, $user_id);
    }

    public static function inGroup($group_name_or_id, $user_id): bool
    {
        if (is_integer($group_name_or_id)) {
            $groups = UsersGroup::where("user_id", $user_id)->where("group_id", $group_name_or_id)->get();
        } else {
            $group = Group::where("name", $group_name_or_id)->first();
            $groups = UsersGroup::where("user_id", $user_id)->where("group_id", $group->id)->get();
        }

        if (sizeof($groups) > 0) {
            return true;
        }

        return false;
    }

    public static function getImageWithSizeGreaterThan($html, $size = 200)
    {
        ini_set('allow_url_fopen', 1);
        $html_parser = new \Yangqi\Htmldom\Htmldom();
        $html_parser->str_get_html($html);

        $featured_img = "";

        $imgs = $html_parser->find('img');

        foreach ($imgs as $img) {

            try {
                [$width, $height] = getimagesize($img->src);

                if ($width >= $size) {
                    $featured_img = $img->src;
                    break;
                }
            } catch (\Exception $e) {
                continue;
            }

        }

        return $featured_img;
    }

    public static function getImageFromString($html, $index = 0)
    {
        $html_parser = new \Yangqi\Htmldom\Htmldom();
        $html_parser->str_get_html($html);
        return isset($html_parser->find('img')[$index]) ? $html_parser->find('img')[$index]->src : '';
    }

    public static function doubleTruncate($val, $f = "0")
    {
        if (($p = strpos($val, '.')) !== false) {
            $val = floatval(substr($val, 0, $p + 1 + $f));
        }
        return $val;
    }

    public static function generateResetCode()
    {

        $code = Crypt::encrypt(str_random(12));

        if (DB::table('users')->where("reset_password_code", $code)->count() > 0) {
            self::generateResetCode();
        }

        return $code;
    }

    public static function imageUpload($file, $folder = null)
    {

        $timestamp = uniqid();
        $ext = $file->guessClientExtension();
        $name = $timestamp . "_file." . $ext;

        if (is_null($folder)) {

            // move uploaded file from temp to uploads directory
            if ($file->move(public_path() . '/uploads/', $name)) {
                return URL::to('/uploads/' . $name);
            }

        } else {


            if (!\File::exists(public_path() . '/uploads/' . $folder)) {
                \File::makeDirectory(public_path() . '/uploads/' . $folder);
            }

            // move uploaded file from temp to uploads directory
            if ($file->move(public_path() . '/uploads/' . $folder . '/', $name)) {
                return URL::to('/uploads/' . $folder . '/' . $name);
            }
        }

        return false;

    }

    public static function findOrString($cat, $col_key)
    {
        return DB::table('settings')->where('category', $cat)->where('column_key', $col_key)->count() > 0 ? Setting::where('category', $cat)->where('column_key', $col_key)->first()->value_string : '';
    }

    public static function findOrTxt($cat, $col_key)
    {
        return DB::table('settings')->where('category', $cat)->where('column_key', $col_key)->count() > 0 ? Setting::where('category', $cat)->where('column_key', $col_key)->first()->value_txt : '';
    }

    public static function findOrCheck($cat, $col_key)
    {
        return DB::table('settings')->where('category', $cat)->where('column_key', $col_key)->count() > 0 ? Setting::where('category', $cat)->where('column_key', $col_key)->first()->value_check : 0;
    }

    public static function setOrCreateSettings($category, $key, $value, $type): void
    {
        if (sizeof(Setting::where('category', $category)->where('column_key', $key)->get()) > 0) {

            $setting = Setting::where('category', $category)->where('column_key', $key)->first();

            if ($type == Setting::TYPE_TEXT) {
                $setting->value_txt = $value;
            }

            if ($type == Setting::TYPE_STRING) {
                $setting->value_string = $value;
            }

            if ($type == Setting::TYPE_CHECK) {
                $setting->value_check = $value;
            }

            $setting->save();

        } else {

            $settings = new Settings();
            $settings->category = $category;
            $settings->column_key = $key;

            if ($type == Setting::TYPE_TEXT) {
                $settings->value_txt = $value;
            }

            if ($type == Setting::TYPE_STRING) {
                $settings->value_string = $value;
            }

            if ($type == Setting::TYPE_CHECK) {
                $settings->value_check = $value;
            }

            $settings->save();
        }
    }

    public static function getSettings($key)
    {

        if ($key == Setting::CATEGORY_CUSTOM_CSS) {
            //Custom CSS Tab
            $settings_custom_css = new StdClass();
            $settings_custom_css->custom_css = self::findOrTxt(Setting::CATEGORY_CUSTOM_CSS, Setting::CATEGORY_CUSTOM_CSS);
            return $settings_custom_css;
        }

        if ($key == Setting::CATEGORY_CUSTOM_JS) {
            //Custom JS Tab
            $settings_custom_js = new StdClass();
            $settings_custom_js->custom_js = self::findOrTxt(Setting::CATEGORY_CUSTOM_JS, Setting::CATEGORY_CUSTOM_JS);
            return $settings_custom_js;
        }

        if ($key == Setting::CATEGORY_SOCIAL) {
            //Social Tab
            $settings_social = new StdClass();
            $settings_social->fb_page_url = self::findOrString(Setting::CATEGORY_SOCIAL, 'fb_page_url');
            $settings_social->twitter_handle = self::findOrString(Setting::CATEGORY_SOCIAL, 'twitter_handle');
            $settings_social->twitter_url = self::findOrString(Setting::CATEGORY_SOCIAL, 'twitter_url');
            $settings_social->google_plus_page_url = self::findOrString(Setting::CATEGORY_SOCIAL, 'google_plus_page_url');
            $settings_social->skype_username = self::findOrString(Setting::CATEGORY_SOCIAL, 'skype_username');
            $settings_social->youtube_channel_url = self::findOrString(Setting::CATEGORY_SOCIAL, 'youtube_channel_url');
            $settings_social->vimeo_channel_url = self::findOrString(Setting::CATEGORY_SOCIAL, 'vimeo_channel_url');
            $settings_social->addthis_js = self::findOrTxt(Setting::CATEGORY_SOCIAL, 'addthis_js');
            $settings_social->sharethis_js = self::findOrTxt(Setting::CATEGORY_SOCIAL, 'sharethis_js');
            $settings_social->sharethis_span_tags = self::findOrTxt(Setting::CATEGORY_SOCIAL, 'sharethis_span_tags');
            $settings_social->facebook_box_js = self::findOrTxt(Setting::CATEGORY_SOCIAL, 'facebook_box_js');
            $settings_social->twitter_box_js = self::findOrTxt(Setting::CATEGORY_SOCIAL, 'twitter_box_js');
            $settings_social->show_sharing = self::findOrCheck(Setting::CATEGORY_SOCIAL, 'show_sharing');
            $settings_social->show_big_sharing = self::findOrCheck(Setting::CATEGORY_SOCIAL, 'show_big_sharing');
            return $settings_social;
        }

        if ($key == Setting::CATEGORY_COMMENTS) {
            //Comments Tab
            $settings_comments = new StdClass();
            $settings_comments->comment_system = self::findOrString(Setting::CATEGORY_COMMENTS, 'comment_system');
            $settings_comments->fb_js = self::findOrTxt(Setting::CATEGORY_COMMENTS, 'fb_js');
            $settings_comments->fb_num_posts = self::findOrString(Setting::CATEGORY_COMMENTS, 'fb_num_posts');
            $settings_comments->fb_comment_count = self::findOrCheck(Setting::CATEGORY_COMMENTS, 'fb_comment_count');
            $settings_comments->disqus_js = self::findOrTxt(Setting::CATEGORY_COMMENTS, 'disqus_js');
            $settings_comments->disqus_comment_count = self::findOrCheck(Setting::CATEGORY_COMMENTS, 'disqus_comment_count');
            $settings_comments->show_comment_box = self::findOrCheck(Setting::CATEGORY_COMMENTS, 'show_comment_box');

            return $settings_comments;
        }

        if ($key == Setting::CATEGORY_SEO) {
            //SEO Tab
            $settings_seo = new StdClass();
            $settings_seo->seo_keywords = self::findOrTxt(Setting::CATEGORY_SEO, 'seo_keywords');
            $settings_seo->seo_description = self::findOrTxt(Setting::CATEGORY_SEO, 'seo_description');
            $settings_seo->google_verify = self::findOrString(Setting::CATEGORY_SEO, 'google_verify');
            $settings_seo->bing_verify = self::findOrString(Setting::CATEGORY_SEO, 'bing_verify');

            return $settings_seo;
        }

        if ($key == Setting::CATEGORY_OLD_NEWS) {
            //Old News Tab
            $settings_old_news = new StdClass();
            $settings_old_news->days = self::findOrString(Setting::CATEGORY_OLD_NEWS, 'days');
            $settings_old_news->auto_update = self::findOrCheck(Setting::CATEGORY_OLD_NEWS, 'auto_delete_old_news');

            return $settings_old_news;
        }

        if ($key == Setting::CATEGORY_GENERAL) {
            //General Tab
            $settings_general = new StdClass();
            $settings_general->site_url = self::findOrString(Setting::CATEGORY_GENERAL, 'site_url');
            $settings_general->site_title = self::findOrString(Setting::CATEGORY_GENERAL, 'site_title');
            $settings_general->analytics_code = self::findOrTxt(Setting::CATEGORY_GENERAL, 'analytics_code');
            $settings_general->mailchimp_form = self::findOrTxt(Setting::CATEGORY_GENERAL, 'mailchimp_form');
            $settings_general->logo_76 = self::findOrString(Setting::CATEGORY_GENERAL, 'logo_76');
            $settings_general->logo_120 = self::findOrString(Setting::CATEGORY_GENERAL, 'logo_120');
            $settings_general->logo_152 = self::findOrString(Setting::CATEGORY_GENERAL, 'logo_152');
            $settings_general->favicon = self::findOrString(Setting::CATEGORY_GENERAL, 'favicon');
            $settings_general->site_post_as_titles = self::findOrCheck(Setting::CATEGORY_GENERAL, 'site_post_as_titles');
            $settings_general->generate_sitemap = self::findOrCheck(Setting::CATEGORY_GENERAL, 'generate_sitemap');
            $settings_general->generate_rss_feeds = self::findOrCheck(Setting::CATEGORY_GENERAL, 'generate_rss_feeds');
            $settings_general->include_sources = self::findOrCheck(Setting::CATEGORY_GENERAL, 'include_sources');

            $locale = self::findOrString(Setting::CATEGORY_GENERAL, 'locale');
            $timezone = self::findOrString(Setting::CATEGORY_GENERAL, 'timezone');

            $settings_general->locale = strlen($locale) <= 0 ? 'en' : $locale;
            $settings_general->timezone = strlen($timezone) <= 0 ? 'America/New_York' : $timezone;

            return $settings_general;
        }

        return [];
    }

    public static function logs(string $str, $arg, $lvl = 'info'): void
    {
        Log::$lvl($str . ' :: ' . print_r($arg, true));
    }

    public static function log($arg, $lvl = 'info'): void
    {
        Log::$lvl(print_r($arg, true));
    }


    public static function prettyDate($date, $time = true): string
    {
        $format = $time ? "F jS, Y" : "F jS, Y";
        return date($format, strtotime($date));
    }

    public static function generateRandom($length = 9, $strength = 4): string
    {

        $vowels = 'aeiouy';
        $consonants = 'bcdfghjklmnpqrstvwxz';
        if ($strength & 1) {
            $consonants .= 'BCDFGHJKLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEIOUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

    public static function messages($v): string
    {
        return implode('', $v->messages()->all('<li style="margin-left:10px;">:message</li>'));
    }


}
