<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment
 *
 * @property int $id
 * @property string $stripe_api_key
 * @property string $stripe_api_secret
 * @property string $rate_per_post
 * @property string $rate_per_source
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Payment extends Model
{
    protected $table = 'payments';

    protected $hidden = [
        'stripe_api_secret'
    ];

    protected $fillable = [
        'stripe_api_key',
        'stripe_api_secret',
        'rate_per_post',
        'rate_per_source'
    ];
}
