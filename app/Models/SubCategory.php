<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SubCategory
 *
 * @property int $id
 * @property int $parent_id
 * @property int $priority
 * @property string $title
 * @property string $slug
 * @property string $scroll_type
 * @property int $show_in_menu
 * @property int $show_in_sidebar
 * @property int $show_in_footer
 * @property string $seo_keywords
 * @property string $seo_description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */



class SubCategory extends Model
{
    protected $table = 'sub_categories';

    protected $casts = [
        'parent_id' => 'int',
        'priority' => 'int',
        'show_in_menu' => 'int',
        'show_in_sidebar' => 'int',
        'show_in_footer' => 'int'
    ];

    protected $fillable = [
        'parent_id',
        'priority',
        'title',
        'slug',
        'scroll_type',
        'show_in_menu',
        'show_in_sidebar',
        'show_in_footer',
        'seo_keywords',
        'seo_description'
    ];

    public const SCROLL_TYPE_PAGINATION = "pagination";
    public const SCROLL_TYPE_SCROLL = "infinite_scroll";
}
