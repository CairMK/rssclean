<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class PostLike
 *
 * @property int $id
 * @property int $post_id
 * @property string $name
 * @property string $email
 * @property int $user_id
 * @property int $rating
 * @property int $approved
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Post $post
 * @property User $user
 *
 * @package App\Models
 */
class PostLike extends Model
{
    protected $table = 'post_likes';

    protected $casts = [
        'post_id' => 'int',
        'user_id' => 'int',
        'rating' => 'int',
        'approved' => 'int'
    ];

    protected $fillable = [
        'post_id',
        'name',
        'email',
        'user_id',
        'rating',
        'approved'
    ];

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
