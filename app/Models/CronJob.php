<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CronJob
 *
 * @property int $id
 * @property string $cron_started_on
 * @property string $cron_completed_on
 * @property string $what
 * @property string $result
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class CronJob extends Model
{
    protected $table = 'cron_jobs';

    protected $fillable = [
        'cron_started_on',
        'cron_completed_on',
        'what',
        'result'
    ];
}
