<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ad
 *
 * @property int $id
 * @property string $code
 * @property string $position
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */



class Ad extends Model
{
    protected $table = 'ads';

    protected $fillable = [
        'code',
        'position'
    ];

    public const TYPE_INDEX_HEADER = "index_header";
    public const TYPE_INDEX_FOOTER = "index_footer";
    public const TYPE_SIDEBAR = "sidebar";
    public const TYPE_ABOVE_POST = "above_post";
    public const TYPE_BELOW_POST = "below_post";
    public const TYPE_BETWEEN_CATEGORY_INDEX = "between_category_index";
    public const TYPE_BETWEEN_SUBCATEGORY_INDEX = "between_sub_category_index";
    public const TYPE_BETWEEN_AUTHOR_INDEX = "between_author_index";
    public const TYPE_BETWEEN_TAG_INDEX = "between_tag_index";
    public const TYPE_BETWEEN_SEARCH_INDEX = "between_search_index";
    public const TYPE_ABOVE_PAGE = "above_page";
    public const TYPE_BELOW_PAGE = "below_page";
}
