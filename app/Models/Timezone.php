<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Timezone
 *
 * @property int $id
 * @property string $country_iso
 * @property string $code
 *
 * @package App\Models
 */
class Timezone extends Model
{
    protected $table = 'timezones';
    public $timestamps = false;

    protected $fillable = [
        'country_iso',
        'code'
    ];
}
