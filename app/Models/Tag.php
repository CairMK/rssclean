<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Tag
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property int $views
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Post[] $posts
 *
 * @package App\Models
 */
class Tag extends Model
{
    protected $table = 'tags';

    protected $casts = [
        'views' => 'int'
    ];

    protected $fillable = [
        'title',
        'slug',
        'views'
    ];

    /**
     * @return BelongsToMany
     */
    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'post_tags')
                    ->withPivot('id', 'views')
                    ->withTimestamps();
    }
}
