<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Category
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property int $parent_id
 * @property string $scroll_type
 * @property int $show_in_menu
 * @property int $show_in_sidebar
 * @property int $show_in_footer
 * @property string $seo_keywords
 * @property string $seo_description
 * @property int $show_as_mega_menu
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $show_on_home
 *
 * @property Collection|Post[] $posts
 * @property Collection|Source[] $sources
 *
 * @package App\Models
 */



class Category extends Model
{
    protected $table = 'categories';

    protected $casts = [
        'parent_id' => 'int',
        'show_in_menu' => 'int',
        'show_in_sidebar' => 'int',
        'show_in_footer' => 'int',
        'show_as_mega_menu' => 'int',
        'show_on_home' => 'int'
    ];

    protected $fillable = [
        'title',
        'slug',
        'parent_id',
        'scroll_type',
        'show_in_menu',
        'show_in_sidebar',
        'show_in_footer',
        'seo_keywords',
        'seo_description',
        'show_as_mega_menu',
        'show_on_home'
    ];

    public function posts(): HasMany
    {
        return $this->hasMany(Post::class);
    }

    public function sources(): HasMany
    {
        return $this->hasMany(Source::class);
    }

    public const SCROLL_TYPE_PAGINATION = "pagination";
    public const SCROLL_TYPE_SCROLL = "infinite_scroll";
}
