<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Setting
 *
 * @property int $id
 * @property string $category
 * @property string $column_key
 * @property string|null $value_string
 * @property string|null $value_txt
 * @property int|null $value_check
 * @property int|null $show_sharing
 * @property int|null $show_big_sharing
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */

class Setting extends Model
{
    protected $table = 'settings';

    protected $casts = [
        'value_check' => 'int',
        'show_sharing' => 'int',
        'show_big_sharing' => 'int'
    ];

    protected $fillable = [
        'category',
        'column_key',
        'value_string',
        'value_txt',
        'value_check',
        'show_sharing',
        'show_big_sharing'
    ];

    public const CATEGORY_CUSTOM_CSS = "custom_css";
    public const CATEGORY_CUSTOM_JS = "custom_js";
    public const CATEGORY_SOCIAL = "social";
    public const CATEGORY_COMMENTS = "comments";
    public const CATEGORY_SEO = "seo";
    public const CATEGORY_GENERAL = "general";
    public const CATEGORY_OLD_NEWS = "old_news";

    public const TYPE_STRING = 'string';
    public const TYPE_TEXT = 'text';
    public const TYPE_CHECK = 'check';
}
