<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class UsersGroup
 *
 * @property int $id
 * @property int $user_id
 * @property int $group_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Group $group
 * @property User $user
 *
 * @package App\Models
 */
class UsersGroup extends Model
{
    protected $table = 'users_groups';

    protected $casts = [
        'user_id' => 'int',
        'group_id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'group_id'
    ];

    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
