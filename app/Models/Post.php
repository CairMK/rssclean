<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Post
 *
 * @property int $id
 * @property int $author_id
 * @property string $title
 * @property string $slug
 * @property string $link
 * @property int $featured
 * @property int $category_id
 * @property string $type
 * @property int $source_id
 * @property string $description
 * @property string $featured_image
 * @property int $views
 * @property int $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $show_in_mega_menu
 * @property string $render_type
 * @property string $video_embed_code
 * @property int $image_parallax
 * @property int $video_parallax
 * @property int $rating_box
 * @property int $show_featured_image_in_post
 * @property int $show_author_box
 * @property int $show_author_socials
 * @property int $dont_show_author_publisher
 * @property int $show_post_source
 * @property string $rating_desc
 *
 * @property User $user
 * @property Category $category
 * @property Source $source
 * @property Collection|ImageGallery[] $image_galleries
 * @property Collection|PostLike[] $post_likes
 * @property Collection|PostRating[] $post_ratings
 * @property Collection|Tag[] $tags
 *
 * @package App\Models
 */



class Post extends Model
{
    protected $table = 'posts';

    protected $casts = [
        'author_id' => 'int',
        'featured' => 'int',
        'category_id' => 'int',
        'source_id' => 'int',
        'views' => 'int',
        'status' => 'int',
        'show_in_mega_menu' => 'int',
        'image_parallax' => 'int',
        'video_parallax' => 'int',
        'rating_box' => 'int',
        'show_featured_image_in_post' => 'int',
        'show_author_box' => 'int',
        'show_author_socials' => 'int',
        'dont_show_author_publisher' => 'int',
        'show_post_source' => 'int'
    ];

    protected $fillable = [
        'author_id',
        'title',
        'slug',
        'link',
        'featured',
        'category_id',
        'type',
        'source_id',
        'description',
        'featured_image',
        'views',
        'status',
        'show_in_mega_menu',
        'render_type',
        'video_embed_code',
        'image_parallax',
        'video_parallax',
        'rating_box',
        'show_featured_image_in_post',
        'show_author_box',
        'show_author_socials',
        'dont_show_author_publisher',
        'show_post_source',
        'rating_desc'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function source(): BelongsTo
    {
        return $this->belongsTo(Source::class);
    }

    public function image_galleries(): HasMany
    {
        return $this->hasMany(ImageGallery::class);
    }

    public function post_likes(): HasMany
    {
        return $this->hasMany(PostLike::class);
    }

    public function post_ratings(): HasMany
    {
        return $this->hasMany(PostRating::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tags')
                    ->withPivot('id', 'views')
                    ->withTimestamps();
    }

    public const TYPE_SOURCE = "source";
    public const TYPE_MANUAL = "manual";

    public const RENDER_TYPE_TEXT = "text";
    public const RENDER_TYPE_IMAGE = "image";
    public const RENDER_TYPE_GALLERY = "gallery";
    public const RENDER_TYPE_VIDEO = "video";

    public const COMMENT_FACEBOOK = "facebook";
    public const COMMENT_DISQUS = "disqus";

    public const STATUS_HIDDEN = 0;
    public const STATUS_PUBLISHED = 1;
}
