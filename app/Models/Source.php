<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Source
 *
 * @property int $id
 * @property string $url
 * @property int $priority
 * @property int $category_id
 * @property string $channel_title
 * @property string $channel_link
 * @property string $channel_description
 * @property string $channel_language
 * @property string $channel_pubDate
 * @property string $channel_lastBuildDate
 * @property string $channel_generator
 * @property int $auto_update
 * @property int $items_count
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $dont_show_author_publisher
 * @property int $show_post_source
 * @property int $fetch_full_text
 * @property int $use_auto_spin
 *
 * @property Category $category
 * @property Collection|Post[] $posts
 *
 * @package App\Models
 */
class Source extends Model
{
    protected $table = 'sources';

    protected $casts = [
        'priority' => 'int',
        'category_id' => 'int',
        'auto_update' => 'int',
        'items_count' => 'int',
        'dont_show_author_publisher' => 'int',
        'show_post_source' => 'int',
        'fetch_full_text' => 'int',
        'use_auto_spin' => 'int'
    ];

    protected $fillable = [
        'url',
        'priority',
        'category_id',
        'channel_title',
        'channel_link',
        'channel_description',
        'channel_language',
        'channel_pubDate',
        'channel_lastBuildDate',
        'channel_generator',
        'auto_update',
        'items_count',
        'dont_show_author_publisher',
        'show_post_source',
        'fetch_full_text',
        'use_auto_spin'
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function posts(): HasMany
    {
        return $this->hasMany(Post::class);
    }
}
