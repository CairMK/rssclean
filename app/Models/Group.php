<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Group
 *
 * @property int $id
 * @property string $name
 * @property string $permissions
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|User[] $users
 *
 * @package App\Models
 */
class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'name',
        'permissions'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_groups')
                    ->withPivot('id')
                    ->withTimestamps();
    }
}
