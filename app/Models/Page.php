<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Page
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property int $show_in_menu
 * @property int $show_in_sidebar
 * @property int $show_in_footer
 * @property string $seo_keywords
 * @property string $seo_description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $status
 * @property int $author_id
 *
 * @property User $user
 *
 * @package App\Models
 */
class Page extends Model
{
    protected $table = 'pages';

    protected $casts = [
        'show_in_menu' => 'int',
        'show_in_sidebar' => 'int',
        'show_in_footer' => 'int',
        'status' => 'int',
        'author_id' => 'int'
    ];

    protected $fillable = [
        'title',
        'slug',
        'description',
        'show_in_menu',
        'show_in_sidebar',
        'show_in_footer',
        'seo_keywords',
        'seo_description',
        'status',
        'author_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id');
    }
}
