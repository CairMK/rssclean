<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $avatar
 * @property string $birthday
 * @property string $bio
 * @property string $gender
 * @property string $mobile_no
 * @property string $country
 * @property string $timezone
 * @property int $activated
 * @property string|null $activation_code
 * @property Carbon|null $activated_at
 * @property string $fb_url
 * @property string $fb_page_url
 * @property string|null $website_url
 * @property string|null $twitter_url
 * @property string|null $google_plus_url
 *
 * @property Collection|Page[] $pages
 * @property Collection|PostLike[] $post_likes
 * @property Collection|PostRating[] $post_ratings
 * @property Collection|Post[] $posts
 * @property Collection|Group[] $groups
 *
 * @package App\Models
 */

class User extends Authenticatable
{
    protected $table = 'users';

    protected $casts = [
        'email_verified_at' => 'datetime',
        'activated' => 'int',
        'activated_at' => 'datetime'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $fillable = [
        'name',
        'slug',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'avatar',
        'birthday',
        'bio',
        'gender',
        'mobile_no',
        'country',
        'timezone',
        'activated',
        'activation_code',
        'activated_at',
        'fb_url',
        'fb_page_url',
        'website_url',
        'twitter_url',
        'google_plus_url'
    ];
    public const TYPE_ADMIN = "admin";
    public const TYPE_CUSTOMER = "customer";
    public const TYPE_PUBLISHER = "publisher";
    public const TYPE_AUTHOR = "author";

    public const GENDER_MALE = "male";
    public const GENDER_FEMALE = "female";

    public function pages(): HasMany
    {
        return $this->hasMany(Page::class, 'author_id');
    }

    public function post_likes(): HasMany
    {
        return $this->hasMany(PostLike::class);
    }

    public function post_ratings(): HasMany
    {
        return $this->hasMany(PostRating::class);
    }

    public function posts(): HasMany
    {
        return $this->hasMany(Post::class, 'author_id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'users_groups')
                    ->withPivot('id')
                    ->withTimestamps();
    }

    public static function hasPermission($permission)
    {

        $users_group = DB::table('users_groups')->where("user_id", Auth::user()->id)->first();

        $group = Group::find($users_group->group_id);

        if ($group->name == User::TYPE_ADMIN) {
            return true;
        }

        $permissions = explode(",", $group->permissions);

        if (in_array($permission, $permissions)) {
            return true;
        } else {
            return false;
        }

    }

    //getAuthPassword , getAuthIdentifier , getRememberToken , getRememberTokenName, setRememberToken , getEmailForPasswordReset
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier(): mixed
    {
        return $this->id;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword(): string
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken(): ?string
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value): void
    {
        $this->remember_token = $value;
        ;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName(): ?string
    {
        return $this->remember_token;
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset(): string
    {
        return $this->email;
    }


}
