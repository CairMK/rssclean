<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class ImageGallery
 *
 * @property int $id
 * @property int $post_id
 * @property string $image
 * @property int $priority
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Post $post
 *
 * @package App\Models
 */
class ImageGallery extends Model
{
    protected $table = 'image_gallery';

    protected $casts = [
        'post_id' => 'int',
        'priority' => 'int'
    ];

    protected $fillable = [
        'post_id',
        'image',
        'priority'
    ];

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }
}
