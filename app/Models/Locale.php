<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Locale
 *
 * @property int $id
 * @property string $title
 * @property string $code
 *
 * @package App\Models
 */
class Locale extends Model
{
    protected $table = 'locales';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'code'
    ];
}
