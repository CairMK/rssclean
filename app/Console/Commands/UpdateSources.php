<?php

namespace App\Console\Commands;

use App\Libraries\Parser;
use App\Libraries\Utils;
use App\Models\Category;
use App\Models\CronJob;
use App\Models\Group;
use App\Models\Post;
use App\Models\PostTag;
use App\Models\Source;
use App\Models\SubCategory;
use App\Models\Tag;
use App\Models\User;
use App\Models\UsersGroup;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class UpdateSources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-sources';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update sources';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        //Update Sources here



        $sources = Source::all();

        $what = "";

        $result = 1;

        $cron_started_on = Carbon::now();

        $what .= "<h2>Cron Job Started On " . $cron_started_on . " </h2> <br>";
        $what .= "<h4>Sources to update (" . sizeof($sources) . ") </h4> <br><br>";

        foreach ($sources as $index => $source) {

            $new_count = 0;
            $updated_count = 0;

            $what .= ($index + 1) . ". Updating source " . $source->title . "(" . $source->url . ") <br><br>";

            $url = $source->url;

            try {
                $feed = Parser::xml($url);
            } catch (\Exception $e) {
                $result = 0;
                $what .= "<label class='label label-warning'> Unable to fetch data from source reason below  </label> <br><br>";
                $what .= "<div class='well'> " . $e->getMessage() . "  </div> <br><br>";
            }


            $source->channel_language = isset($feed['channel']['language']) ? $feed['channel']['language'] : '';
            $source->channel_pubDate = isset($feed['channel']['pubDate']) ? $feed['channel']['pubDate'] : '';
            $source->channel_lastBuildDate = isset($feed['channel']['lastBuildDate']) ? $feed['channel']['lastBuildDate'] : '';
            $source->channel_generator = isset($feed['channel']['generator']) ? $feed['channel']['generator'] : '';
            $source->items_count = isset($feed['channel']['item']) ? $source->items_count + sizeof($feed['channel']['item']) : $source->items_count;
            $source->save();

            if (isset($feed['channel']['item']) && $source->auto_update == 1) {



                //Post::where('source_id', $source->id)->delete();

                foreach ($feed['channel']['item'] as $item) {

                    if (!is_null($item['title']) && !is_null($item['description']) && !is_null($item['pubDate']) && !is_null($item['link'])) {


                        if (sizeof(Post::where('slug', Str::slug($item['title']))->get()) <= 0) {


                            if ($source->fetch_full_text == 1) {
                                $item['description'] = Parser::fetchFull($item['link'], $item['description']);
                            }

                            if ($source->use_auto_spin == 1) {
                                $arr = Parser::spin($item['title'], $item['description']);
                                //$item['title'] = $arr['title'];
                                $item['description'] = $arr['description'];
                            }

                            $new_count += 1;

                            if (is_array($item['description'])) {
                                $item['description'] = "";
                            }


                            [$item['render_type'], $item['featured_image']] = Parser::setImgAndRenderType($item['description'], $item['video_embed_code'], $item['featured_image']);

                            $user_group = Group::where('name', User::TYPE_ADMIN)->first();

                            $find_id = UsersGroup::where('group_id', $user_group->id)->pluck('user_id');

                            $first_admin = User::where('id', $find_id)->first();


                            $post_item = new Post();
                            $post_item->author_id = $first_admin->id;
                            $post_item->title = $item['title'];
                            $post_item->save();

                            $post_item->slug = (is_null(Str::slug($item['title'])) || empty(Str::slug($item['title']))) ? $post_item->id : Str::slug($item['title']);

                            $post_item->link = $item['link'];
                            $post_item->category_id = $source->category_id;
                            $post_item->featured = 0;
                            $post_item->type = Post::TYPE_SOURCE;
                            $post_item->render_type = $item['render_type'];
                            $post_item->source_id = $source->id;
                            $post_item->description = is_array($item['description']) ? '' : $item['description'];
                            $post_item->featured_image = $item['featured_image'];
                            $post_item->video_embed_code = $item['video_embed_code'];
                            $post_item->dont_show_author_publisher = $source->dont_show_author_publisher;
                            $post_item->show_post_source = $source->show_post_source;
                            $post_item->show_author_box = $source->dont_show_author_publisher == 1 ? 0 : 1;
                            $post_item->show_author_socials = $source->dont_show_author_publisher == 1 ? 0 : 1;
                            $post_item->rating_box = 0;
                            $post_item->created_at = $item['pubDate'];
                            $post_item->views = 1;
                            $post_item->save();


                            if (isset($item['categories'])) {
                                $this->createTags($item['categories'], $post_item->id);
                            }


                        } else {

                            $exists_post = Post::where('slug', Str::slug($item['title']))->first();

                            if ($source->fetch_full_text == 1) {
                                $item['description'] = Parser::fetchFull($item['link'], $item['description']);
                            }

                            if ($source->use_auto_spin == 1) {
                                $arr = Parser::spin($item['title'], $item['description']);
                                $item['title'] = $arr['title'];
                                $item['description'] = $arr['description'];
                            }

                            if (is_array($item['description'])) {
                                $item['description'] = "";
                            }

                            if (is_array($item['title'])) {
                                $item['title'] = "";
                            }


                            $updated_count += 1;
                            $exists_post->render_type = $item['render_type'];
                            $exists_post->link = $item['link'];
                            $exists_post->description = $item['description'];
                            $exists_post->video_embed_code = $item['video_embed_code'];
                            if (empty($exists_post->featured_image = $item['featured_image'])) {
                                $exists_post = Post::selectRaw('featured_image')->first();
                                $exists_post->featured_image;

                            }
                            $exists_post->save();


                            if (isset($item['categories'])) {
                                $this->createTags($item['categories'], $exists_post->id);
                            }
                        }


                    }
                }


            } else {
                $what .= "Source not set to auto update so skipping  " . $source->title . "  <br><br>";
            }

            $what .= "Posts ----- NEW : " . $new_count . "     UPDATED : " . $updated_count . " <br>";

            $what .= "<hr>";
        }

        $old_news = Utils::getSettings('old_news');
        $delete_count = 0;

        if ($old_news->auto_update == 1) {
            $posts = Post::all();

            foreach ($posts as $post) {

                $created_at = $post->created_at;
                $now = Carbon::now();

                if ($now->diffInDays($created_at) >= $old_news->days) {
                    $post->delete();
                    $delete_count++;
                }
            }

            $what .= "Old News is Set to Delete which are " . $old_news->days . " days old : Deleted :" . $delete_count . " Posts which matched the settings <br><br>";
        }

        $cron_completed_on = Carbon::now();

        $what .= "<h2>Finalizing Cron job  </h2> <br><br>";

        $posts = Post::all();

        foreach ($posts as $post) {
            if ($post->type == Post::TYPE_SOURCE && sizeof(SubCategory::where('id', $post->category_id)->get()) <= 0) {
                $post->delete();
            } else {

                $sub = SubCategory::where('id', $post->category_id)->first();

                if (sizeof(Category::where('id', $sub->parent_id)->get()) <= 0) {
                    $post->delete();
                }

            }
        }

        $what .= "<h2>Cron Job Completed On " . $cron_completed_on . " </h2> <br><br>";

        $cron = new CronJob();
        $cron->cron_started_on = $cron_started_on;
        $cron->cron_completed_on = $cron_completed_on;
        $cron->what = $what;
        $cron->result = $result;
        $cron->save();


    }

    public function createTags($tags, $post_id): void
    {

        $post_tags = PostTag::where('post_id', $post_id)->get();

        foreach ($post_tags as $post_tag) {
            Tag::where('id', $post_tag->tag_id)->delete();
        }

        PostTag::where('post_id', $post_id)->delete();

        foreach ($tags as $tag) {

            $old_tag = Tag::where('title', $tag)->first();

            if (isset($old_tag)) {

                $pt = new PostTag();
                $pt->post_id = $post_id;
                $pt->tag_id = $old_tag->id;
                $pt->save();

            } else {
                $new_tag = new Tag();
                $new_tag->title = $tag;
                $new_tag->slug = Str::slug($tag);
                $new_tag->save();

                $pt = new PostTag();
                $pt->post_id = $post_id;
                $pt->tag_id = $new_tag->id;
                $pt->save();
            }
        }


    }
}
